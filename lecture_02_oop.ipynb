{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7ed8199c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Welcome to PPY lecture #2\n",
    "\n",
    "## Introduction to object-oriented programming (OOP) in Python and a case study"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b8d9f697",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Just a few organizational notes\n",
    "\n",
    "1. Quick reminder about the conditions for \"zápočet\"\n",
    "2. If you struggle to install jupyter/jupyterlab (especially on Windows) — [Anaconda](https://www.anaconda.com/download) can help\n",
    "3. If you happen to know anyone, who can not attend the lecture, because of a clash in their timetable — let me know, this is why we are recording lectures"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2223f21e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Object-oriented programming (OOP)\n",
    "\n",
    "... is a programming paradigm that focuses on the creation of objects that contain both data and behavior. Python supports OOP and provides robust support for OOP concepts. Here are the main OOP concepts in Python and their uses:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "baa41ca3",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "1. Classes and Objects: A class is a blueprint for creating objects, while an object is an instance of a class. Classes define the properties and methods that all objects of that class will have. You can use classes and objects in Python to organize your code and create reusable code modules."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78eab2b7",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "2. Inheritance: Inheritance is a way to create a new class based on an existing class. The new class inherits all the properties and methods of the existing class and can add new functionality or modify existing functionality. You can use inheritance in Python to reuse code and create new classes quickly."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38704ec1",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "3. Polymorphism: Polymorphism is the ability of objects of different classes to respond to the same message or method call. In Python, you can achieve polymorphism through method overloading and method overriding."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6d9b565",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "4. Encapsulation: Encapsulation is a way to hide the implementation details of a class from the outside world. In Python, you can use access modifiers like private and protected to control the access to the properties and methods of a class."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "862edd85",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "An small example of how you can use OOP concepts in Python:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "82297f5a",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Toyota\n",
      "Corolla\n",
      "2018\n",
      "Toyota Corolla (2018)\n"
     ]
    }
   ],
   "source": [
    "# Define a class\n",
    "class Car:\n",
    "    def __init__(self, make, model, year):\n",
    "        self.make = make\n",
    "        self.model = model\n",
    "        self.year = year\n",
    "\n",
    "    def get_make(self):\n",
    "        return self.make\n",
    "\n",
    "    def get_model(self):\n",
    "        return self.model\n",
    "\n",
    "    def get_year(self):\n",
    "        return self.year\n",
    "\n",
    "    def __str__(self):\n",
    "        return f\"{self.make} {self.model} ({self.year})\"\n",
    "\n",
    "# Create an object of the class\n",
    "car1 = Car(\"Toyota\", \"Corolla\", 2018)\n",
    "\n",
    "# Access properties and methods of the object\n",
    "print(car1.get_make()) # Output: Toyota\n",
    "print(car1.get_model()) # Output: Corolla\n",
    "print(car1.get_year()) # Output: 2018\n",
    "print(car1) # Output: Toyota Corolla (2018)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ef94e3e",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "In this example, we define a Car class with properties like make, model, and year, and methods like `get_make()`, `get_model()`, and `get_year()`. We also define a `str()` method to provide a string representation of the object. We then create an object of the class (car1) and access its properties and methods."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79ec3c19",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### ... but \"why\"?\n",
    "\n",
    "It is not mandatory to use objects when programming in Python. For some reason, I have seen multiple situations of someone thinking that you are a better programmer if you follow the OOP principles. But IMHO it not always the case (we will get to an alternative approach later). \n",
    "\n",
    "I am convinced that you will become a better programmer only if **you can choose the right tool for the task you are solving**. Let me show you one example where OOP helped me greatly."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b50b5583",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Case study — Heuristics testing framework\n",
    "\n",
    "Let me walk you through [this implementation](https://github.com/matejmojzes/18heur-2020/tree/master/src)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82643770",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "And BTW, an interesting article on the topic topic of OOP vs functional programming, as the alternative: https://towardsdatascience.com/python-to-oop-or-to-fp-13ac79a43b16."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f377ed8c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Inspiration for your own work — Shapes\n",
    "\n",
    "Create a Shape class that represents a two-dimensional shape, with subclasses like Circle, Square, and Rectangle. Each subclass should have properties like the length of its sides, its area, and its perimeter. You can also define methods like `resize()` and `draw()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "789a501c",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Create some shapes\n",
    "circle = Circle(5)\n",
    "square = Square(10)\n",
    "rectangle = Rectangle(10, 5)\n",
    "\n",
    "# Get the area and perimeter of each shape\n",
    "print(\"Circle area:\", circle.area()) # Output: Circle area: 78.53981633974483\n",
    "print(\"Circle perimeter:\", circle.perimeter()) # Output: Circle perimeter: 31.41592653589793\n",
    "print(\"Square area:\", square.area()) # Output: Square area: 100\n",
    "print(\"Square perimeter:\", square.perimeter()) # Output: Square perimeter: 40\n",
    "print(\"Rectangle area:\", rectangle.area()) # Output: Rectangle area: 50\n",
    "print(\"Rectangle perimeter:\", rectangle.perimeter()) # Output: Rectangle perimeter: 30\n",
    "\n",
    "# Resize the shapes\n",
    "circle.resize(2)\n",
    "square.resize(0.5)\n",
    "rectangle.resize(1.5)\n",
    "\n",
    "# Draw the shapes (not really graphical plots... yet!)\n",
    "circle.draw() # Output: Drawing shape\n",
    "square.draw() # Output: Drawing shape\n",
    "rectangle.draw() # Output: Drawing shape"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
