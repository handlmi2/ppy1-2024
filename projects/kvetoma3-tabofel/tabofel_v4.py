# Goal:
# 1. Apply some sort of data formats (.csv) and functions associated with them
# 2. Use Pandas to create tables and graphs of various relations in the dataset
# 3. Implement standards used in functional programming, f.e. lambda functions and generators
# 4. Proper docstrings following learned conventions

import csv
import pandas as pd
import os
from colorama import Fore, Style
import random
import argparse
import matplotlib.pyplot as plt
import math

def get_path(file_name: str) -> str:

    """
    Returns relative path. It is crucial for the dataset to be in the same directory as the script.
    """

    script_dir = os.path.dirname(os.path.abspath(__file__))
    file_path = os.path.join(script_dir, file_name)

    return file_path

#########################################################################  

def element_write(shortcut: str) -> None:

    """
    Function 'element_write' appends new element into the .csc file. 
    String variable {shortcut} specifies the element which is being appended.
    If the given element is already in the table, function raises RuntimeError. 
    """

    if element_exists(shortcut): raise RuntimeError(f"Element {shortcut} is already in the table.") 

    lst = [
        "shortcut",
        "czech_name",
        "latin_name", 
        "english_name", 
        "atomic_mass", 
        "atomic_number", 
        "mass_number", 
        "electronegativity", 
        "preferred_oxidation_state", 
        "group", 
        "group_in_old_format", 
        "period"
        ]

    output_dict = {'shortcut' : shortcut}

    # Start at 1 compensates for the added shortcut in the lst
    # The program does not ask user again about the element shortcut
    for i in range(1,len(lst)):

        print(lst[i])
        user_input = str(input())
        output_dict.update({lst[i]:user_input})

    path = get_path('tabofel_#2.csv')

    with open(path, 'a', newline='') as csv_element:

        writer = csv.DictWriter(csv_element, fieldnames=lst, )
        writer.writerow(output_dict)

#########################################################################

def table_read() -> list:

    """
    Function 'table_read' returns the list of all elements currently in the table.
    Element is the dictionary type.
    """

    path = get_path('tabofel_#2.csv')

    with open(path, 'r+') as csv_elements:

        csv_reader = csv.DictReader(csv_elements)

        elements = [row for row in csv_reader]

    return elements

#########################################################################

def is_string(string: str) -> bool:

    return isinstance(string, str)

#########################################################################

def get_element(shortcut: str) -> dict:

    """
    Function 'get_element' returns the characteristics of one element specified by the string variable {shortcut}.
    Returned value is a dictionary.
    If the element is not in the dataset, function raises a RuntimeError.
    """
    
    if not is_string(shortcut): raise TypeError(f"Instead of a str, an {str(type(shortcut).__name__)} has been given.")

    elements = table_read()
    desired = None

    # Creates list with one dictionary in it
    for el in elements:

        if el["shortcut"] == shortcut: desired = el

    if desired == None: raise RuntimeError(f"Element {shortcut} is not in the table of elements.")

    # Returns the only element in the list, which is the first one
    return desired

#########################################################################

def element_exists(shortcut: str) -> bool:

    """
    Function 'element_exists' returns True if the element is in the dataset, otherwise returns False.
    Takes the string variable {shortcut}.
    """

    elements = table_read()

    for el in elements:

        if el['shortcut'] == shortcut: return True

    return False
        
#########################################################################

def element_print(shortcut: str) -> None:

    """
    Function 'element_print' visualise the characteristics of an element on the screen.
    Takes one string argument {shortcut}.
    """

    element = get_element(shortcut)
    for key, value in element.items():

        print(f"{key} : {value}")

#########################################################################

def order_by(char: str, pos: int, reversed = False) -> list:

    """
    Function 'order_by' returns the element on the given position {pos} in the list of elements sorted by selected character {char}.
    If the {char} is not one of the characteristics of the element, fuction raises ValueError.
    """

    lst = [
        "shortcut",
        "czech_name",
        "latin_name", 
        "english_name", 
        "atomic_mass", 
        "atomic_number", 
        "mass_number", 
        "electronegativity", 
        "preferred_oxidation_state", 
        "group", 
        "period"
        ]    

    if char not in lst:

        raise ValueError(f"Characteristic {char} is not a valid option.")

    # List of all elements in the table
    # Each element is a dictionary
    table = table_read()

    return sorted(table, key = lambda element: element[char], reverse=reversed)[pos]

#########################################################################

def get_two_characters_table(character1: str, character2: str, sort_order = 'asc') -> pd.DataFrame:

    """
    Function 'get_two_characters_table' returns the data frame.
    Data frame has 4 columns. The first contains enumeration of the rows. The second contains shortcut of each element. The last two are reserved for the {character1} and {character2}.
    """
    data = {}
    elements = table_read()
    float_characters = ["atomic_mass", "atomic_number", "mass_number", "electronegativity", "preferred_oxidation_state", "group", "period"]

    # Additional condition used for sorting
    # Sometimes the character1 should be integer and not a string, the list bellow contains all of those situations
    new_char1 = [float(el[character1]) for el in elements] if character1 in float_characters else [el[character1] for el in elements]
    new_char2 = [float(el[character2]) for el in elements] if character2 in float_characters else [el[character2] for el in elements]

    if sort_order != 'asc' and sort_order != 'desc':

        raise SyntaxError(f"Wrong attribute in the 'table' command given. Use '?' for help.") 

    # Creating the dict for data frame
    data = {
        'shortcut' : [el['shortcut'] for el in elements],
        character1 : new_char1,
        character2 : new_char2,
    }
    
    return pd.DataFrame(data).sort_values(by = character1, ascending = (sort_order == 'asc'))

#########################################################################

def get_graph(character1: str, character2: str, exclude_nobel = False) -> None:

    """
    Creates .png file with the graph of two characteristics in relation to each other.
    """

    frame = get_two_characters_table(character1, character2)
    integer_required = ["group","atomic_number","mass_number","period","preferred_oxidation_state"]

    if exclude_nobel:
        frame = frame.query('shortcut not in ["He", "Ne", "Ar", "Kr", "Xe", "Rn", "Og"]')

    plt.scatter(character2, character1,  data=frame)
    plt.show()

#########################################################################

def molmass(compound: str) -> float:

    """
    Returns the molar mass of given 'compound' round to the 4 decimal places. Identifies the individual elements by distinguishing between capital and lower case letters.
    Stores the elements and their count in the pair of lists. It is crucial to control the input.
    """

    # Is the letter capital or not
    def key_function_capital(letter: str) -> bool:

        return ord(letter) <= 90 and ord(letter) >= 65

    def key_function_number(letter: str) -> bool:

        return ord(letter) <= 57 and ord(letter) >= 48

    # 'compound_list' contains shortcut of each element given in the 'compound' variable
    compound_list = []

    # 'multiplicator' contains number of atoms each element has in the 'compound' variable 
    multiplicator = []
    number = ""
    element = ""
    
    # Cycle transforms the string variable 'compound' into a 'compound_list' and 'multiplicator'
    for letter in compound:
        if key_function_capital(letter) and element != "":
            compound_list.append(element)
            multiplicator.append(number)
            number = ""
            element = letter
        
        elif key_function_number(letter) and element != "":
            number += letter

        else:
            element += letter

    # The last element in the string was not added in the cycle, add it here
    compound_list.append(element)
    multiplicator.append(number)

    #replacing the empty string with 1 and transforming str numbers into actuall numbers
    for i in range(len(multiplicator)):

        if multiplicator[i] == "":
            multiplicator[i] = 1

        else:
            multiplicator[i] = int(multiplicator[i])

    mol = 0
    # Iterating on the 'compound_list', counting the molar mass of each element and multiplicating it using values in the 'multiplicator'
    for i in range(len(compound_list)):

        el_dict = get_element(compound_list[i])
        mol += float(el_dict["atomic_mass"]) * multiplicator[i]

    # For the sake of nice format
    return round(mol,4)

#########################################################################

def table_of_elements() -> None:

    """
    Function 'table_of_elements' uses multiple functions to perform various operations using dataset tabofel_#4.csv.
    Uses parser to run the program in the terminal.
    """

    if __name__ == "__main__":

        parser = argparse.ArgumentParser(
            prog='Tabofel',
            description='Uses the database of chemical elements to extract various data.',
            epilog='The chemistry is an art.'
        )

        # dest is the value, where all the subcommands will be stored
        subparsers = parser.add_subparsers(dest='command', help="sub_command help")
        allowed_list = ["electronegativity", "atomic_mass", "atomic_number", "mass_number", "preferred_oxidation_state", "group", "period"]

        parser_write = subparsers.add_parser('write', help='writes the desired element into the database')
        parser_write.add_argument('-s', '--shortcut', required=True, type=str, help="shortcut of desired element")

        parser_read = subparsers.add_parser('read', help='prints the desired element')
        parser_read.add_argument('-s', '--shortcut', required=True, type=str, help="shortcut of desired element")

        parser_sort = subparsers.add_parser('sort', help='prints the n-th element in the sorted databse')
        parser_sort.add_argument('-pos', '--position', required=True, type=int, help="desired position of an element in the sorted row")
        parser_sort.add_argument('-srt', '--sortby', required=True, type=str, choices=allowed_list, help="characteristic of an element - the elements are sorted depend in relation to this characteristic")
        parser_sort.add_argument('-rev', '--reverse', required=False, type=bool, choices=[True, False], help='Sort in ascending (False) or descending (True) order.')

        parser_graph = subparsers.add_parser('graph', help='creates the graph of two characteristics.')
        parser_graph.add_argument('-y', '--dependant', required=True, type=str, choices=allowed_list, help="characteristic of an elements - y axis in the graph (dependant on the second characteristic)")
        parser_graph.add_argument('-x', '--independant', required=True, type=str, choices=allowed_list, help="characteristic of an elements - x axis in the graph (independant characteristic)")
        parser_graph.add_argument('-ex', '--exnob', required=False, type=bool, choices=[True, False], help="exclude nobel gases (True) or not (False)")

        parser_table = subparsers.add_parser('table', help='creates the dataset with two characteristics of all elements')
        parser_table.add_argument('-y', '--dependant', required=True, type=str, choices=allowed_list, help="characteristic of an elements - y axis in the graph (dependant on the second characteristic)")
        parser_table.add_argument('-x', '--independant', required=True, type=str, choices=allowed_list, help="characteristic of an elements - x axis in the graph (independant characteristic)")
        parser_table.add_argument('-o', '--order', required=True, type=str, choices=['desc', 'asc'], help="descending (desc) or ascending (asc) order.")

        parser_molmass = subparsers.add_parser('molmass', help='computes the molar mass of the given compound.')
        parser_molmass.add_argument('-c', '--compound', required=True, type=str, help="molecular formula of the chemical compound, f.e.: C6H12O6, HgSO4, C2H6COOH")

        # Method parse_args runs the parser and places the extracted data in the argparse.Namespace object
        args = parser.parse_args()

        # Using the argparse.Namespace object and dest value to call the subcommand
        command = args.command

        match command:

            case "write":

                element_write(args.shortcut)
                print(f"Element {args.shortcut} succesfully added.")

            case "read":

                element_print(args.shortcut)

            case "sort":

                element = order_by(args.sortby, args.position, args.reverse)
                for key, value in element.items():
                    print(f"{key} : {value}")

            case "graph":

                get_graph(args.dependant, args.independant, args.exnob)

            case "table":

                print(get_two_characters_table(args.dependant, args.independant, args.order))

            case "molmass":

                print(molmass(args.compound))

            case _:

                raise SyntaxError(f"Invalid command inserted.")

#########################################################################

table_of_elements()
