A = [
    [41, 5],
    [2, 3]
]

def transpose (arr: list):
    return[*zip(*arr)]

def max_row(arr: list):
    return max([sum(x) for x in arr])

def max_col(arr: list):
    return max([sum(x) for x in transpose(arr)])

def normal_max(arr: list) -> int:
    vals = []
    for x in arr:
        vals.append(sum(x))
    result = max(vals)
    return result

print(f"Maximum sum of any row is: '{max_row(A)}'")
print(f"Maximum sum of any col is: '{max_col(A)}'")
