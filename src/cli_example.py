import click

# This decorator creates a group of commands (square and greet)
@click.group()
def cli():
    """A simple CLI example"""
    pass

@click.command()
@click.option('--count', default=1, help='Number of greetings')
@click.option('--name', prompt='Your name', help='The person to greet')
def greet(count: int, name: str) -> None:
    """Greet the user COUNT times"""
    for _ in range(count):
        click.echo(f'Hello, {name}!')

@click.command()
@click.option('--number', default=1, help='The number to square')
def square(number: int) -> None:
    """Calculates the square of NUMBER"""
    result = number * number
    click.echo(f'The square of {number} is {result}')

cli.add_command(greet)
cli.add_command(square)

if __name__ == '__main__':
    cli()
